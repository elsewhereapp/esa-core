package com.dreamcloud.esa_core.vectorizer.scoreMod;

import com.dreamcloud.esa_core.vectorizer.VectorizationOptions;
import com.dreamcloud.esa_score.score.TfIdfScore;

import java.util.Vector;

public class PruneScoreMod extends ScoreMod {
    private int windowSize;
    private float windowDrop;

    public PruneScoreMod(int windowSize, float windowDrop) {
        super(ScoreModPosition.PRE_VECTORIZATION, ScoreModApplication.TERM);
        this.windowSize = windowSize;
        this.windowDrop = windowDrop;
    }

    public float getWindowDrop() {
        return windowDrop;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public void setWindowDrop(float windowDrop) {
        this.windowDrop = windowDrop;
    }

    @Override
    public Vector<TfIdfScore> applyMod(Vector<TfIdfScore> termScores) {
        if (windowSize <= 0) {
            return termScores;
        }

        Vector<TfIdfScore> allTermScores = new Vector<>();
        for (int scoreIdx = 0; scoreIdx < termScores.size(); scoreIdx++) {
            allTermScores.add(termScores.get(scoreIdx));
            if (scoreIdx + windowSize < termScores.size()) {
                float headScore = (float) termScores.get(scoreIdx).getScore();
                float tailScore = (float) termScores.get(scoreIdx + windowSize).getScore();
                if (headScore - tailScore < headScore * windowDrop) {
                    break;
                }
            }
        }
        return allTermScores;
    }
}
