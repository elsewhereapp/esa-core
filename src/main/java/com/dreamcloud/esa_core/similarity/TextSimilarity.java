package com.dreamcloud.esa_core.similarity;

public interface TextSimilarity {
     SimilarityInfo score(String doc1, String doc2) throws Exception;
     SimilarityInfo score(String doc1, String doc2, boolean gatherTopConcepts) throws Exception;
}
