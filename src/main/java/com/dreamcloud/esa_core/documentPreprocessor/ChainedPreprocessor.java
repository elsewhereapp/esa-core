package com.dreamcloud.esa_core.documentPreprocessor;

import java.util.ArrayList;
import java.util.Collection;

public class ChainedPreprocessor implements DocumentPreprocessor {
    Collection<DocumentPreprocessor> preprocessors;
    public ChainedPreprocessor(Collection<DocumentPreprocessor> preprocessors) {
        this.preprocessors = preprocessors;
    }

    public ChainedPreprocessor() {
        this.preprocessors = new ArrayList<>();
    }

    public void addPreprocessor(DocumentPreprocessor preprocessor) {
        this.preprocessors.add(preprocessor);
    }

    public String process(String document) throws Exception {
        for (DocumentPreprocessor preprocessor: preprocessors) {
            document = preprocessor.process(document);
        }
        return document;
    }

    public String getInfo() {
        ArrayList<String> preprocessorNames = new ArrayList<>();
        for (DocumentPreprocessor preprocessor: preprocessors) {
            preprocessorNames.add(preprocessor.getInfo());
        }
        return String.join(", ", preprocessorNames);
    }
}
